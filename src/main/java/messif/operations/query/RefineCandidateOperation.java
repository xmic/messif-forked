/*
 *  This file is part of MESSIF library.
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operations.query;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.ArrayBlockingQueue;
import messif.objects.LocalAbstractObject;
import messif.objects.util.AbstractObjectIterator;
import messif.operations.AbstractOperation;
import messif.operations.AnswerType;
import messif.operations.GetCandidateSetOperation;
import messif.operations.OperationErrorCode;
import messif.operations.QueryOperation;
import messif.operations.RankingMultiQueryOperation;
import messif.operations.RankingQueryOperation;
import messif.operations.RankingSingleQueryOperation;

/**
 * This operation starts refinement of the encapsulated {@link RankingSingleQueryOperation} using
 *  the candidate set stored in the provided {@link GetCandidateSetOperation}.
 * 
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
@AbstractOperation.OperationName("query that reranks given list of objects (IDs) with respect to a query object (and returns 'k' best)")
public class RefineCandidateOperation extends RankingQueryOperation {

    /** Class id for serialization. */
    private static final long serialVersionUID = 332401L;
    /**
     * Operation encapsulating the candidate set.
     */
    private final GetCandidateSetOperation candidateOperation;
    
    /**
     * Operation to be refined using the given candidate set.
     */
    private final RankingQueryOperation operationToRefine;
    
    /**
     * Creates the operation given a {@link RankingSingleQueryOperation} to be refined and the candidate set
     * within a {@link GetCandidateSetOperation}.
     *
     * @param candidateOperation encapsulation of the candidate set
     * @param operationToRefine ranking operation to be refined
     */
    public RefineCandidateOperation(GetCandidateSetOperation candidateOperation, RankingQueryOperation operationToRefine) {
        super(operationToRefine.getAnswerType());
        this.candidateOperation = candidateOperation;
        this.operationToRefine = operationToRefine;
        copyParameters(operationToRefine, true);        
    }    

    /**
     * Creates the operation given a query object, "k", list of locators and answer type.
     *  A new {@link KNNQueryOperation} is created and stored as {@link #operationToRefine};
     *  new {@link GetCandidateSetOperation} is created and filled with the specified list of IDs.
     * @param query query object
     * @param k "k"
     * @param objLocators list of locators that form the candidate set
     * @param answerType answer object type
     */
    protected RefineCandidateOperation(LocalAbstractObject query, int k, Collection<String> objLocators, AnswerType answerType) {
        super(answerType);
        if (objLocators == null || objLocators.isEmpty()) {
            throw new IllegalArgumentException("list of locators musn't be empty");
        }
        this.operationToRefine = new KNNQueryOperation(query, k, answerType);
        this.candidateOperation = new GetCandidateSetOperation((KNNQueryOperation) operationToRefine, objLocators.size(), new ArrayBlockingQueue<>(objLocators.size(), true, objLocators));
        this.candidateOperation.endOperation();
    }
    
    /**
     * Creates the operation given a query object and other parameters specified by {@link KNNRankByLocatorOperation}.
     *  A new {@link KNNQueryOperation} is created and stored as {@link #operationToRefine};
     *  new {@link GetCandidateSetOperation} is created and filled with the specified list of IDs.
     * @param query query object
     * @param knnRankOperation
     */
    public RefineCandidateOperation(LocalAbstractObject query, KNNRankByLocatorOperation knnRankOperation) {
        this(query, knnRankOperation.getK(), knnRankOperation.getObjLocators(), knnRankOperation.getAnswerType());
        copyParameters(knnRankOperation, true);
    }
    
    /**
     * Creates the operation given a query object, "k", list of locators and answer type.
     *  A new {@link KNNQueryOperation} is created and stored as {@link #operationToRefine};
     *  new {@link GetCandidateSetOperation} is created and filled with the specified list of IDs.
     * @param query query object
     * @param k "k"
     * @param objLocators list of locators that form the candidate set
     * @param answerType answer object type
     */
    @AbstractOperation.OperationConstructor({"query object", "k", "list of object locators to rerank", "answer type"})
    public RefineCandidateOperation(LocalAbstractObject query, int k, String [] objLocators, AnswerType answerType) {
        this(query, k, Arrays.asList(objLocators), answerType);
    }
    
    @AbstractOperation.OperationConstructor({"query object", "k", "list of object locators to rerank"})
    public RefineCandidateOperation(LocalAbstractObject query, int k, String [] objLocators) {
        this(query, k, objLocators, AnswerType.NODATA_OBJECTS);
    }
    
    /**
     * Get the candidate operation.
     * @return the candidate operation.
     */
    public GetCandidateSetOperation getCandidateOperation() {
        return candidateOperation;
    }
    
    /**
     * Returns the ranking operation to be refined.
     * @return the ranking operation to be refined.
     */
    public RankingQueryOperation getRankingOperation() {
        return operationToRefine;
    }
    
    // *****************************        Overrides         ********************************* //
    
    @Override
    public Object getArgument(int index) throws IndexOutOfBoundsException {
        switch (index) {
        case 0:
            return getRankingOperation();
        case 1:
            return getCandidateOperation().getCandidateSetSize();
        case 2:
            return getRankingOperation().getAnswerCount();
        case 3:
            return getRankingOperation().getAnswerType();
        default:
            throw new IndexOutOfBoundsException(RefineCandidateOperation.class.getName() + "has only four argument");
        }
    }

    @Override
    public int getArgumentCount() {
        return 4;
    }

    @Override
    public String getArgumentString(int index) throws IndexOutOfBoundsException, UnsupportedOperationException {
        if (index == 0 && (getRankingOperation() instanceof RankingSingleQueryOperation || getRankingOperation() instanceof RankingMultiQueryOperation)) {
            return getRankingOperation().getArgumentString(index);
        }
        return super.getArgumentString(index);
    }
    
    @Override
    public void updateFrom(RankingQueryOperation operation) throws ClassCastException {
        if (this == operation) {
            return;
        }
        if (operation instanceof RefineCandidateOperation) {
            this.operationToRefine.updateFrom(((RefineCandidateOperation) operation).getRankingOperation());
        } else {
            this.operationToRefine.updateFrom(operation);
        }
    }
    
    @Override
    public boolean wasSuccessful() {
        return isErrorCode(OperationErrorCode.RESPONSE_RETURNED);
    }

    @Override
    public void endOperation() {
        endOperation(OperationErrorCode.RESPONSE_RETURNED);
    }

    @Override
    public int evaluate(AbstractObjectIterator objects) {
        return operationToRefine.evaluate(objects);
    }

    @Override
    public Class getAnswerClass() {
        return operationToRefine.getAnswerClass();
    }

    @Override
    public int getAnswerCount() {
        return operationToRefine.getAnswerCount();
    }

    @Override
    public Iterator getAnswer() {
        return operationToRefine.getAnswer();
    }

    @Override
    public Iterator getAnswer(int skip, int count) {
        return operationToRefine.getAnswer(skip, count);
    }

    @Override
    public Iterator getAnswerObjects() {
        return operationToRefine.getAnswerObjects();
    }

    @Override
    public void resetAnswer() {
        operationToRefine.resetAnswer();
    }

    @Override
    public int getSubAnswerCount() {
        return operationToRefine.getSubAnswerCount();
    }

    @Override
    public Iterator getSubAnswer(int index) throws IndexOutOfBoundsException {
        return operationToRefine.getSubAnswer(index);
    }

    @Override
    public Iterator getSubAnswer(Object key) {
        return operationToRefine.getSubAnswer(key);
    }

    @Override
    protected boolean dataEqualsImpl(QueryOperation operation) {
        return operationToRefine.dataEquals(operation);
    }

    @Override
    public int dataHashCode() {
        return operationToRefine.dataHashCode();
    }

}
